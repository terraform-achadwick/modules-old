variable "pub_key_path" {
  type    = string
}

variable "blueprint_id" {
  type = string
}

variable "bundle_id" {
  type = string
}

variable "region" {
  type = string
}

variable "project_name" {
  type    = string
}