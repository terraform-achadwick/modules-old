output "instance_public_ip" {
  value = aws_lightsail_instance.ls_instance.id
}

output "ls_static_ip_address" {
  value = aws_lightsail_static_ip.ls_static_ip.ip_address
}