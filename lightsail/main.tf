#Import existing public key
resource "aws_lightsail_key_pair" "ls_key_pair" {
  name       = "${var.project_name}-ls-keypair"
  public_key = file(var.pub_key_path)
}

#Create static ip
resource "aws_lightsail_static_ip" "ls_static_ip" {
  name = "${var.project_name}-ls-static-ip"
}

# Create a new Lightsail Instance
resource "aws_lightsail_instance" "ls_instance" {
  name              = "${var.project_name}-ls-instance"
  availability_zone = "${var.region}a"
  blueprint_id      = var.blueprint_id
  bundle_id         = var.bundle_id
  key_pair_name     = "${var.project_name}-ls-keypair"
}

resource "aws_lightsail_static_ip_attachment" "ls_static_ip_attachment" {
  static_ip_name = aws_lightsail_static_ip.ls_static_ip.id
  instance_name  = aws_lightsail_instance.ls_instance.id
}