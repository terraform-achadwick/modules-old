data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "dwick-terraform-state"
    key = "${var.environment}/vpc/terraform.tfstate"
    region = "eu-west-2"
  }
}

resource "aws_instance" "example" {
  ami           = lookup(var.ami_ubuntu_2004, var.region)
  instance_type = var.instance_type
  subnet_id = data.terraform_remote_state.vpc.outputs.subnet_0

  tags = {
    "Name" = "${var.project_name}-ec2-0"
    "project" = "terraform"
  }
}