variable "project_name" {
  type    = string
}

variable "environment" {
  type    = string
}

variable "region" {
  type    = string
}

variable "instance_type" {
  type    = string
}

variable "ami_ubuntu_2004" {
  type = map
  default = {
    "eu-west-2" = "ami-0b1feeb6a46bffd83"
    "us-west-2" = "ami-06e54d05255faf8f6"
  }
}