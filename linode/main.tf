#Create linode instance
resource "linode_instance" "instance" {
  label           = "${var.project_name}-instance"
  image           = var.linode_image
  region          = var.region
  type            = var.linode_type
  authorized_keys = [chomp(file("${var.key_path}.pub"))] #removes any trailing \n
  root_pass       = var.root_pass

#  config {
#    label = "attach-existing-volume"
#    devices {
#      sdb {
#        volume_id = data.local_file.clone_volume_id.content
#      }
#    }
#  }

  tags = [ var.project_name ]
}