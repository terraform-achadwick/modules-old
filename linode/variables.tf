variable "key_path" {
  type    = string
}

variable "root_pass" {
  type    = string
}

variable "linode_image" {
  type = string
}

variable "linode_type" {
  type = string
}

variable "region" {
  type = string
}

variable "project_name" {
  type    = string
}